package com.example.desarrollo3.primeraapp.core;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.example.desarrollo3.primeraapp.presentation.activity.WelcomeActivity;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class BaseFragment extends Fragment {
    //// ---> Abstraemos el código reutilizable para fragments


    // Iniciar una actividad
    public void startNewActivity(Context context, Class<?> cls){
        // pasamos como parámetros del intent el contexto y
        // el nombre de la clase a donde nos dirijimos
        Intent intent = new Intent(context, cls);
        // jalamos la actividad principal y luego se inicia la actividad
        getActivity().startActivity(intent);
    }
}
