package com.example.desarrollo3.primeraapp.presentation.fragments;



import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.desarrollo3.primeraapp.R;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class ItemTresFragment extends Fragment {
    // Singleton
    public static ItemTresFragment newInstance(){
        return new ItemTresFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // true cierra la app
        View view = inflater.inflate(R.layout.item_viewpager_landing_3, container , false);
        return view;

    }
}
