package com.example.desarrollo3.primeraapp.presentation.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.example.desarrollo3.primeraapp.R;
import com.example.desarrollo3.primeraapp.core.BaseActivity;
import com.example.desarrollo3.primeraapp.presentation.fragments.LandingFragment;
import com.example.desarrollo3.primeraapp.presentation.fragments.WelcomeFragment;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class WelcomeActivity extends BaseActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        setFragmentToActivity(R.id.bodywelcome, WelcomeFragment.newInstance());

    }
}
