package com.example.desarrollo3.primeraapp.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.desarrollo3.primeraapp.R;
import com.example.desarrollo3.primeraapp.core.BaseFragment;
import com.example.desarrollo3.primeraapp.presentation.activity.WelcomeActivity;
import com.example.desarrollo3.primeraapp.presentation.adapters.LandingPagerAdapter;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class LandingFragment extends BaseFragment {

    @BindView(R.id.vp_landing)
    ViewPager vpLanding;
    @BindView(R.id.btn_next)
    Button btnNext;
    LandingPagerAdapter mAdapter;

    // Singleton
    public static LandingFragment newInstance() {
        return new LandingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // true cierra la app
        View v = inflater.inflate(R.layout.fragment_landing, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Creamos la lista de fragments
        ArrayList<Fragment> list = new ArrayList<>();
        //añadimos valores a la lista
        list.add(ItemUnoFragment.newInstance());
        list.add(ItemDosFragment.newInstance());
        list.add(ItemTresFragment.newInstance());

        // iniciamos el adaptador
        mAdapter = new LandingPagerAdapter(getFragmentManager(), list);
        // le pasamos a la vista el adaptador
        vpLanding.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_next)
    public void onClick() {
        // iniciamos la nueva actividad
        System.out.println("HIIII");
        startNewActivity(getContext(),WelcomeActivity.class);
    }
}
